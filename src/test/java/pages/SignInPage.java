package pages;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignInPage extends BasePage {

    @FindBy(xpath = "//input[@id='userid']")
    private WebElement userIdInputTextField;

    @FindBy(xpath = "//p[@id='signin-error-msg']")
    private WebElement signInErrorMessageParagraph;

    @FindBy(xpath = "//button[@id='signin-continue-btn']")
    private WebElement signInContinueButton;


    public SignInPage(WebDriver driver) {
        super(driver);
    }





    public void typeTextInUserIdInputTextField(String userId) {
        userIdInputTextField.sendKeys(userId);
    }

    public void clickOnSignInContinueButton() {
        signInContinueButton.click();
    }

    public WebElement getSignInErrorMessageParagraph() {
        return signInErrorMessageParagraph;
    }


    public WebElement getUserIdInputTextField() {
        return userIdInputTextField;
    }


    public boolean ifSignInErrorMessageParagraphExist() {

        try {

            signInErrorMessageParagraph.getText();
            return true;

        } catch (NoSuchElementException exception) {
            return false;
        }


    }


}

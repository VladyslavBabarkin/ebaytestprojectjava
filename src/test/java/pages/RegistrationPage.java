package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class RegistrationPage extends BasePage {


    @FindBy(xpath = "//input[@id='firstname']")
    private WebElement firstNameInputTextField;

    @FindBy(xpath = "//input[@id='lastname']")
    private WebElement lastNameInputTextField;

    @FindBy(xpath = "//input[@id='email']")
    private WebElement eMailInputTextField;

    @FindBy(xpath = "//input[@id='PASSWORD']")
    private WebElement passwordInputField;

    @FindBy(xpath = "//button[@id='ppaFormSbtBtn']")
    private WebElement createAccountSubmitButton;

    @FindBy(xpath = "//input[@aria-label='Show password']")
    private WebElement showPasswordCheckbox;


    public RegistrationPage(WebDriver driver) {
        super(driver);
    }


    public WebElement getFirstNameInputTextField() {
        return firstNameInputTextField;
    }

    public WebElement getLastNameInputTextField() {
        return lastNameInputTextField;
    }

    public WebElement getEmailInputTextField() {
        return eMailInputTextField;
    }

    public WebElement getPasswordInputField() {
        return passwordInputField;
    }

    public WebElement getCreateAccountSubmitButton() {
        return createAccountSubmitButton;
    }

    public void clickOnShowPasswordCheckbox() {
        showPasswordCheckbox.click();
    }

    public boolean ifPasswordInputFieldTypeIsPassword() {
        return passwordInputField.getAttribute("type").equals("password");

    }

    public boolean ifPasswordInputFieldTypeIsText() {
        return passwordInputField.getAttribute("type").equals("text");
    }


    public String getPasswordValueFromPasswordInputField() {
        return passwordInputField.getAttribute("value");
    }

    public void typeTextInPasswordInputField(String password) {
        passwordInputField.sendKeys(password);
    }

    public boolean ifShowPasswordCheckboxIsChecked() {
        return showPasswordCheckbox.isSelected();
    }


}
package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.math.BigDecimal;
import java.sql.Driver;

public class ProductItemPage extends BasePage {

    /*
    @FindBy(xpath = "//span[@id='mm-saleOrgPrc']")
    private WebElement wasPriceSpan;

    @FindBy(xpath = "//div[@id='mm-saleAmtSavedPrc']")
    private WebElement youSaveDiscountDiv;

    @FindBy(xpath = "//span[@id='mm-saleDscPrc']")
    private WebElement isPriceSpan;
*/

    @FindBy(xpath = "//span[@id='orgPrc']")
    private WebElement wasPriceSpan;

    @FindBy(xpath = "//span[@id='youSaveSTP']")
    private WebElement youSaveDiscountDiv;

    @FindBy(xpath = "//span[@id='prcIsum']")
    private WebElement isPriceSpan;



    @FindBy(xpath = "//a[@href='https://www.ebay.com/']")
    private WebElement ebayGoToHomePageImgLogo;


    public ProductItemPage(WebDriver driver) {
        super(driver);
    }

    public BigDecimal getWasPriceBigDecimal() {

        return new BigDecimal(wasPriceSpan.getText().replace(" ", "").replace("$", "").replace("US", ""));
    }

    public BigDecimal getYouSaveDiscountBigDecimal() {
        String youSaveDiscountFieldValue = youSaveDiscountDiv.getAttribute("innerHTML");
        return new BigDecimal(youSaveDiscountFieldValue.substring(youSaveDiscountFieldValue.indexOf('$') + 1, youSaveDiscountFieldValue.indexOf('(')).replace(" ", ""));
    }

    public BigDecimal getIsPriceBigDecimal() {
        return new BigDecimal(isPriceSpan.getText().replace(" ", "").replace("$", "").replace("US", ""));
    }


    public void clickEbayGoToHomePageImgLogo() {
        ebayGoToHomePageImgLogo.click();
    }


}

package pages;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ShoppingCartPage extends BasePage {


    @FindBy(xpath = "//div[@class='empty-cart']")
    WebElement emptyCartDiv;

    @FindBy(xpath = "//div[@data-test-id='cart-bucket']")
    WebElement cartFulfilledWithItemsDiv;

    @FindBy(xpath = "//div[@aria-live='polite']/span/span/span")
    WebElement emptyCartInfoMessageSpan;


    public ShoppingCartPage(WebDriver driver) {
        super(driver);
    }


    public boolean ifEmptyCartDivExists() {
        try {

            emptyCartDiv.getText();
            return true;

        } catch (NoSuchElementException exception) {
            return false;
        }

    }


    public boolean ifCartFulfilledWithItemsDivExists() {
        try {

            cartFulfilledWithItemsDiv.getText();
            return true;

        } catch (NoSuchElementException exception) {
            return false;
        }

    }


    public WebElement getEmptyCartInfoMessageSpan() {
        return emptyCartInfoMessageSpan;
    }
}

package pages;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SearchResultsPage extends BasePage {

    @FindBy(xpath = "//span[contains(@class,'pageheader')]")
    private WebElement searchCategoryPageHeaderSpan;


    public SearchResultsPage(WebDriver driver) {
        super(driver);
    }


    public boolean ifSearchCategoryPageHeaderSpanExists() {
        try {

            searchCategoryPageHeaderSpan.getText();
            return true;

        } catch (NoSuchElementException exception) {
            return false;
        }

    }

    public WebElement getSearchCategoryPageHeaderSpan() {
        return searchCategoryPageHeaderSpan;
    }


}

package pages;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
public class AdvancedSearchPage extends BasePage{

    @FindBy(xpath = "//input[@name='_udlo']")
    private WebElement priceRangeMinimumValueInputTextField;

    @FindBy(xpath = "//input[@name='_udhi']")
    private WebElement priceRangeMaximumValueInputTextField;

    @FindBy(xpath = "//input[@name='_nkw']")
    private WebElement keywordSearchInputTextField;


    public AdvancedSearchPage(WebDriver driver) {
        super(driver);
    }

    public WebElement getPriceRangeMinimumValueInputTextField() {
        return priceRangeMinimumValueInputTextField;
    }
    public WebElement getPriceRangeMaximumValueInputTextField() {
        return priceRangeMaximumValueInputTextField;
    }

    public WebElement getKeywordSearchInputTextField(){return keywordSearchInputTextField;}

}

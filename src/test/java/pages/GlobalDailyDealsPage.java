package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class GlobalDailyDealsPage extends BasePage {


    @FindBy(xpath = "//div[@class= 'ebayui-dne-item-featured-card ebayui-dne-item-featured-card']//div[@class='col']//span[@class='itemtile-price-bold']")
    private List<WebElement> featuredDealsListItemsWithDiscount;


    public GlobalDailyDealsPage(WebDriver driver) {
        super(driver);
    }

    public List<WebElement> getFeaturedDealsItemsWithDiscountList() {
        return featuredDealsListItemsWithDiscount;
    }


}

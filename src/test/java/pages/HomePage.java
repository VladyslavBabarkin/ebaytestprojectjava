package pages;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class HomePage extends BasePage {

    @FindBy(xpath = "//span[@id='gh-ug']//a[contains(@href,'https://signin.ebay.com')]")
    private WebElement signInButton;

    @FindBy(xpath = "//a[contains(@href,'https://reg.ebay.com')]")
    private WebElement registerButton;

    @FindBy(xpath = "//li[@class='hl-cat-nav__js-tab']//a[text()='Electronics']")
    private WebElement mainCategoriesElectronicsButton;

    @FindBy(xpath = "//a[text()='Camera Drones']")
    private WebElement mainCategoriesElectronicsCameraDronesButton;

    @FindBy(xpath = "//div[@id='gh-top']//a[contains(text(),'Daily Deals')]")
    private WebElement daylyDealsButton;

    @FindBy(xpath = "//input[@id='gh-btn']")
    private WebElement searchButton;

    @FindBy(xpath = "//select[@id='gh-cat']")
    private WebElement searchCategorySelect;

    @FindBy(xpath = "//div[@class='carousel__viewport' and preceding-sibling::button[contains(@aria-label,'Your Recently Viewed Items')]]//ul/li")
    List<WebElement> recentlyViewedItemsList;

    @FindBy(xpath = "//div[@class='carousel__viewport' and preceding-sibling::button[contains(@aria-label,'Daily Deals')]]//ul/li")
    List<WebElement> dailyDealsItemsList;

    @FindBy(xpath = "//a[contains(@href,'https://cart.payments.ebay.com/')]")
    WebElement shoppingCart;

    @FindBy(xpath = "//a[@id='gh-as-a']")
    WebElement advancedSearchButton;


    public HomePage(WebDriver driver) {
        super(driver);
    }


    public void clickOnSignInButton() {
        signInButton.click();
    }

    public void clickOnRegisterButton() {
        registerButton.click();
    }

    public WebElement getMainCategoriesElectronicsButton() {
        return mainCategoriesElectronicsButton;
    }


    public WebElement getMainCategoriesElectronicsCameraDronesButton() {
        return mainCategoriesElectronicsCameraDronesButton;
    }

    public void clickOnMainCategoriesElectronicsCameraDronesButton() {
        mainCategoriesElectronicsCameraDronesButton.click();
    }

    public void clickOnDailyDealsButton() {
        daylyDealsButton.click();
    }

    public void clickOnSearchButton() {
        searchButton.click();
    }


    public WebElement getSearchCategorySelect() {
        return searchCategorySelect;
    }


    public List<WebElement> getRecentlyViewedItemsList() {
        return recentlyViewedItemsList;
    }

    public boolean ifRecentlyViewedItemsListExists() {

        try {
            recentlyViewedItemsList.size();
            return true;

        } catch (NoSuchElementException exception) {
            return false;
        }

    }

    public List<WebElement> getDailyDealsItemsList() {
        return dailyDealsItemsList;
    }

    public void clickOnShoppingCart() {
        shoppingCart.click();


    }


    public void clickOnAdvancedSearchButton() {
        advancedSearchButton.click();
    }

}

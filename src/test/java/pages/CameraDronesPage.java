package pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.List;

public class CameraDronesPage extends BasePage {

    @FindBy(xpath = "//input[@aria-label='Free International Shipping']")
    private WebElement freeInternationalShippingCheckbox;


    @FindBy(xpath = "//ul[contains(@class,'b-list__items_nofooter')]//span[contains(@class,'logisticsCost')]")
    private List<WebElement> shoppingPickListLogisticsCostsValuesList;

    @FindBy(xpath = "//ul[contains(@class,'b-list__items_nofooter')]//span[@class='s-item__price']")
    private List<WebElement> shoppingPickListPriceValuesList;

    @FindBy(xpath = "//input[contains(@class,'input--from')]")
    private WebElement priceRangeMinimumValueInputTextField;

    @FindBy(xpath = "//input[contains(@class,'input--to')]")
    private WebElement priceRangeMaximumValueInputTextField;


    @FindBy(xpath = "//div[@class='x-price__error show']")
    private WebElement incorrectPriceErrorMessageDiv;


    public CameraDronesPage(WebDriver driver) {
        super(driver);
    }


    public void clickOnFreeInternationalShippingCheckbox() {
        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].click();", freeInternationalShippingCheckbox);
    }


    public List<WebElement> getShoppingPickListLogisticsCostsValuesList() {
        return shoppingPickListLogisticsCostsValuesList;
    }


    public void setPriceRangeMinimumValue(double minimumPrice) {
        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].value='" + minimumPrice + "';", priceRangeMinimumValueInputTextField);
    }

    public void setPriceRangeMaximumValue(double maximumPrice) {

        ((JavascriptExecutor) getDriver()).executeScript("arguments[0].value='" + maximumPrice + "';", priceRangeMaximumValueInputTextField);
    }


    public WebElement getPriceRangeMaximumValueInputTextField() {
        return priceRangeMaximumValueInputTextField;
    }


    public class PriceRange {

        public double priceFrom;
        public double priceTo;

        public PriceRange(double priceFrom, double priceTo) {
            this.priceFrom = priceFrom;
            this.priceTo = priceTo;
        }

    }


    public ArrayList<PriceRange> getShoppingPickListItemsPriceRangeList() {

        ArrayList<PriceRange> priceList = new ArrayList<PriceRange>();

        for (WebElement pickListItemPrice : shoppingPickListPriceValuesList) {

            String singleItemPrice = pickListItemPrice.getAttribute("innerHTML");

            if (singleItemPrice.contains("<span class=\"DEFAULT\"> to </span>")) {

                String priceFromString = singleItemPrice.substring(0, singleItemPrice.indexOf("<"));
                String priceToString = singleItemPrice.substring(singleItemPrice.lastIndexOf(">") + 1);

                Double priceToDouble = Double.parseDouble(priceToString.replace(" ", "").replace("\"", "").replace("$", ""));
                Double priceFromDouble = Double.parseDouble(priceFromString.replace(" ", "").replace("\"", "").replace("$", ""));

                priceList.add(new PriceRange(priceFromDouble,priceToDouble));


            } else {
                Double priceDouble = Double.parseDouble(singleItemPrice.replace("\"", "").replace("$", ""));
                priceList.add(new PriceRange(priceDouble, priceDouble));
            }


        }
        return priceList;

    }


    public boolean ifIncorrectPriceErrorMessageDivExist() {

        try {

            incorrectPriceErrorMessageDiv.getText();
            return true;

        } catch (NoSuchElementException exception) {
            return false;
        }


    }

    public String getIncorrectPriceErrorMessageString() {

        return incorrectPriceErrorMessageDiv.getText();


    }


}

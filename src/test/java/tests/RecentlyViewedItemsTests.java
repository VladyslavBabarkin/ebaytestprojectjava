package tests;

import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Assert;
import org.testng.annotations.Test;

public class RecentlyViewedItemsTests extends BaseTest {


    @Test(priority = 7)
    public void checkBrowsedItemAppersInRecentlyViewedItemsList() {

        if (!getHomePage().ifRecentlyViewedItemsListExists()) {
            Assert.fail("Recently Viewed Items List shouldn't exist for a New User with no browsed items");
        } else {

            getHomePage().getDailyDealsItemsList().get(0).click();

            String browsedDailyDealItemUrl = getDriver().getCurrentUrl().substring(0, getDriver().getCurrentUrl().indexOf('?'));
            getProductItemPage().clickEbayGoToHomePageImgLogo();
            Assert.assertTrue(getDriver().getCurrentUrl().equals("https://www.ebay.com/"));


            if (getHomePage().ifRecentlyViewedItemsListExists()) {
                getHomePage().getRecentlyViewedItemsList().get(0).click();
                String recentlyViewedItemUrl = getDriver().getCurrentUrl().substring(0, getDriver().getCurrentUrl().indexOf('?'));
                Assert.assertEquals(recentlyViewedItemUrl, browsedDailyDealItemUrl);

            } else {
                Assert.fail("Recently Viewed Items List is not found.  Recently Viewed Items List should exist after a New User browsed on particular item");
            }

        }


    }


}

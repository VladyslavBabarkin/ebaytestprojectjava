package tests;

import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class ProductItemCardTests extends BaseTest {


    @Test(priority = 8)
    public void checkCorrectFinalIsPriceShownAfterPreviousWasPriceMinusYouSaveDiscountSubtracted() {

        getHomePage().clickOnDailyDealsButton();

        List<WebElement> featuredDealsItemsWithDiscountList = getGlobalDailyDealsPage().getFeaturedDealsItemsWithDiscountList();

        if (featuredDealsItemsWithDiscountList != null && featuredDealsItemsWithDiscountList.size() >= 1) {
            featuredDealsItemsWithDiscountList.get(getBasePage().getRandomItemNumber(featuredDealsItemsWithDiscountList.size())).click();

        }

        Assert.assertEquals(getProductItemPage().getIsPriceBigDecimal(), getProductItemPage().getWasPriceBigDecimal().subtract(getProductItemPage().getYouSaveDiscountBigDecimal()));
    }


}

package tests;

import org.testng.Assert;
import org.testng.annotations.Test;

public class RegistrationTests extends BaseTest {

    private String EXPECTED_TEST_PASSWORD = "TEST_passw0rd";


    @Test(priority = 5)
    public void checkCreateAccountSubmitButtonIsDisabledIfAllCredentialsEmpty() {

        getHomePage().clickOnRegisterButton();
        getBasePage().waitVisibilityOfElement(30, getRegistrationPage().getFirstNameInputTextField());
        Assert.assertTrue(getBasePage().getValueAttribute(getRegistrationPage().getFirstNameInputTextField()).isEmpty());
        Assert.assertTrue(getBasePage().getValueAttribute(getRegistrationPage().getLastNameInputTextField()).isEmpty());
        Assert.assertTrue(getBasePage().getValueAttribute(getRegistrationPage().getEmailInputTextField()).isEmpty());
        Assert.assertTrue(getBasePage().getValueAttribute(getRegistrationPage().getPasswordInputField()).isEmpty());
        Assert.assertFalse(getRegistrationPage().getCreateAccountSubmitButton().isEnabled());
    }

    @Test(priority = 6)
    public void checkPasswordFieldMakesPasswordVisibleForUserWhileChoosingShowPasswordOptionInRegistrationForm() {

        getHomePage().clickOnRegisterButton();
        getBasePage().waitVisibilityOfElement(30, getRegistrationPage().getFirstNameInputTextField());
        Assert.assertTrue(getRegistrationPage().ifPasswordInputFieldTypeIsPassword());
        Assert.assertFalse(getRegistrationPage().ifShowPasswordCheckboxIsChecked());
        getRegistrationPage().typeTextInPasswordInputField(EXPECTED_TEST_PASSWORD);
        getRegistrationPage().clickOnShowPasswordCheckbox();
        Assert.assertTrue(getRegistrationPage().ifPasswordInputFieldTypeIsText());
        Assert.assertEquals(getRegistrationPage().getPasswordValueFromPasswordInputField(), EXPECTED_TEST_PASSWORD);

    }


}

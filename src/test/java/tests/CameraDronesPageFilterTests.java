package tests;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;
import pages.CameraDronesPage;

import static org.testng.Assert.assertTrue;

public class CameraDronesPageFilterTests extends BaseTest {


    private String FREE_SHIPPING = "Free shipping";
    private double PRICE_RANGE_MIN_VALUE =50;
    private double PRICE_RANGE_MAX_VALUE = 250;

    private double PRICE_RANGE_MIN_INCORRECTVALUE = -1;
    private double PRICE_RANGE_MAX_INCORRECTVALUE = Double.MAX_VALUE;

    private final String INCORRECT_PRICE_WARNING_MESSAGE = "Please provide a valid price range";

    @Test(priority = 9)
    public void checkFilterResultsContainFreeShippingItemsOnly() {
        getBasePage().moveToWebElement(getHomePage().getMainCategoriesElectronicsButton());
        getHomePage().clickOnMainCategoriesElectronicsCameraDronesButton();
        getCameraDronesPage().clickOnFreeInternationalShippingCheckbox();
        for (WebElement webElement : getCameraDronesPage().getShoppingPickListLogisticsCostsValuesList()) {
            Assert.assertTrue(webElement.getText().contains(FREE_SHIPPING));
        }

    }


    @Test(priority = 10)
    public void checkFilterResultsContainItemsWithPriceWithinParticularRange() {


        getBasePage().moveToWebElement(getHomePage().getMainCategoriesElectronicsButton());
        getBasePage().waitVisibilityOfElement(30, getHomePage().getMainCategoriesElectronicsCameraDronesButton());
        getHomePage().clickOnMainCategoriesElectronicsCameraDronesButton();
        getCameraDronesPage().setPriceRangeMinimumValue(PRICE_RANGE_MIN_VALUE);
        getCameraDronesPage().setPriceRangeMaximumValue(PRICE_RANGE_MAX_VALUE);
        getCameraDronesPage().getPriceRangeMaximumValueInputTextField().sendKeys(Keys.ENTER);

        for (CameraDronesPage.PriceRange itemPriceValue : getCameraDronesPage().getShoppingPickListItemsPriceRangeList()) {

            Assert.assertTrue(itemPriceValue.priceFrom<=itemPriceValue.priceTo);
            Assert.assertFalse(itemPriceValue.priceTo <PRICE_RANGE_MIN_VALUE || itemPriceValue.priceFrom > PRICE_RANGE_MAX_VALUE);
        }

    }

    @Test(priority = 11)
    public void checkWarningMessageValueAppearsWhileEnteringIncorrectPriceRangeValues() {


        getBasePage().moveToWebElement(getHomePage().getMainCategoriesElectronicsButton());
        getBasePage().waitVisibilityOfElement(30, getHomePage().getMainCategoriesElectronicsCameraDronesButton());
        getHomePage().clickOnMainCategoriesElectronicsCameraDronesButton();

        if (getCameraDronesPage().ifIncorrectPriceErrorMessageDivExist()) {

            Assert.fail("Price Error message should not be shown");


        } else {
            getCameraDronesPage().setPriceRangeMinimumValue(PRICE_RANGE_MIN_INCORRECTVALUE);
            getCameraDronesPage().setPriceRangeMaximumValue(PRICE_RANGE_MAX_INCORRECTVALUE);
            getCameraDronesPage().getPriceRangeMaximumValueInputTextField().sendKeys(Keys.ENTER);

            Assert.assertTrue(getCameraDronesPage().ifIncorrectPriceErrorMessageDivExist());
            Assert.assertEquals(getCameraDronesPage().getIncorrectPriceErrorMessageString(), INCORRECT_PRICE_WARNING_MESSAGE);

        }


    }


}

package tests;

import org.testng.Assert;
import org.testng.annotations.Test;

public class ShoppingCartTests extends BaseTest {


    private String EXPECTED_EMPTY_CART_INFO_MESSAGE = "You don't have any items in your cart.";

    @Test(priority = 2)
    public void checkShoppingCartIsEmptyForANewUser() {
        getHomePage().clickOnShoppingCart();
        Assert.assertFalse(getShoppingCartPage().ifCartFulfilledWithItemsDivExists());
        Assert.assertTrue(getShoppingCartPage().ifEmptyCartDivExists());
    }

    @Test(priority = 2)
    public void checkEmptyShoppingCartContainsInfoMessageSignalizingAboutItsmptiness() {

        getHomePage().clickOnShoppingCart();

        if (getShoppingCartPage().ifEmptyCartDivExists()) {
            Assert.assertEquals(getShoppingCartPage().getEmptyCartInfoMessageSpan().getText(), EXPECTED_EMPTY_CART_INFO_MESSAGE);
        }
        else
        {
            Assert.fail("EmptyCartDiv with Message Doesn't Exist");
        }


    }


}

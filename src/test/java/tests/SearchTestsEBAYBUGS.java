package tests;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;

public class SearchTestsEBAYBUGS extends BaseTest {

    private String ADVANCED_SEARCH_KEYWORD = "Ebay";
    private String ADVANCED_SEARCH_PRICE_RANGE_MIN_INCORRECTVALUE = "-100";
    private String ADVANCED_SEARCH_PRICE_RANGE_MAX_INCORRECTVALUE = "-10000";

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //SEE "Bug Reports Two Ebay Bugs.docx"
    //This test is going to FAIL because of bug on Ebay while entering minus values into price fields in Advanced Search Page
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Test
    public void checkAdvancedSearchTestWithIncorrectPriceRangeValues() {

        getHomePage().clickOnAdvancedSearchButton();
        String advancedSearchUrl = getDriver().getCurrentUrl();
        getAdvancedSearchPage().getKeywordSearchInputTextField().sendKeys(ADVANCED_SEARCH_KEYWORD);
        getAdvancedSearchPage().getPriceRangeMinimumValueInputTextField().sendKeys(ADVANCED_SEARCH_PRICE_RANGE_MIN_INCORRECTVALUE);
        getAdvancedSearchPage().getPriceRangeMaximumValueInputTextField().sendKeys(ADVANCED_SEARCH_PRICE_RANGE_MAX_INCORRECTVALUE);
        getAdvancedSearchPage().getPriceRangeMaximumValueInputTextField().sendKeys(Keys.ENTER);
        Assert.assertEquals(getDriver().getCurrentUrl(), advancedSearchUrl);


    }


    private String SEARCH_CATEGORY_NAME = "eBay Motors";

    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //SEE "Bug Reports Two Ebay Bugs.docx"
    //This test is going to FAIL because of bug on Ebay while selecting ebay Motors Search Category and leaving empty value in search field
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @Test
    public void checkPageHeaderOnSearchResultsPageEqualsSelectedCategoryNameUsingEmptySearchRequestAndParticularSearchCategoryName() {

        Select searchCategorySelect = new Select(getHomePage().getSearchCategorySelect());
        searchCategorySelect.selectByVisibleText(SEARCH_CATEGORY_NAME);
        getHomePage().clickOnSearchButton();

        Assert.assertTrue(getDriver().getCurrentUrl().contains(SEARCH_CATEGORY_NAME), "URL MISMATCH");

        if (getSearchResultsPage().ifSearchCategoryPageHeaderSpanExists()) {
            String actualPageHeaderName = getSearchResultsPage().getSearchCategoryPageHeaderSpan().getText();

            Assert.assertEquals(actualPageHeaderName, SEARCH_CATEGORY_NAME, "Expected Page Header according to Selected Category Name value mismatches Actual Page Header value");

        } else {
            Assert.fail("Page Header Span '" + SEARCH_CATEGORY_NAME + "' doesn't exist on a Search Results Page");
        }

    }



      ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
      //EXTENDED VERSION OF PREVIOUS TEST checkPageHeaderOnSearchResultsPageEqualsSelectedCategoryNameUsingEmptySearchRequestAndParticularSearchCategoryName
      //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    private class SearchCategoryPageHeadersAndHrefsCorrespondingValues {
        private String searchCategoryName = "";
        private String pageHeader = "";
        private String hrefContains = "";

        public SearchCategoryPageHeadersAndHrefsCorrespondingValues(String searchCategoryName, String pageHeader, String hrefContains) {

            this.searchCategoryName = searchCategoryName;
            this.hrefContains = hrefContains;
            this.pageHeader = pageHeader;

        }


        public String getSearchCategory() {
            return searchCategoryName;
        }

        public String getPageHeader() {
            return pageHeader;
        }

        public String getpartialHref() {
            return hrefContains;
        }
    }


    private ArrayList<SearchCategoryPageHeadersAndHrefsCorrespondingValues> EXPECTED_SEARCH_CATEGORY_PAGE_HEADERS_AND_HREFS_CORRESPONDING_VALUES_LIST = new ArrayList<SearchCategoryPageHeadersAndHrefsCorrespondingValues>() {{

        add(new SearchCategoryPageHeadersAndHrefsCorrespondingValues("All Categories", "All Categories", "All-Categories"));
        //POSSIBLE BUG (OR FEATURE?) ON EBAY. ACTUAL RESULT : add(new SearchCategoryPageHeadersAndHrefsCorrespondingValues("All Categories", "All Categories", "all-categories"));
        //POSSIBLE BUG (OR FEATURE?) ON EBAY. ACTUAL RESULT: ANOTHER LAYOUT ON "All Categories" SEARCH RESULTS PAGE. NOT THE SAME AS IN MOST OTHER CATEGORIES.

        add(new SearchCategoryPageHeadersAndHrefsCorrespondingValues("Antiques", "Antiques", "Antiques"));
        add(new SearchCategoryPageHeadersAndHrefsCorrespondingValues("Art", "Art", "Art"));

        add(new SearchCategoryPageHeadersAndHrefsCorrespondingValues("Baby Essentials", "Baby Essentials", "Baby-Essentials"));
        //POSSIBLE BUG (OR FEATURE?) ON EBAY. ACTUAL RESULT : add(new SearchCategoryPageHeadersAndHrefsCorrespondingValues("Baby", "Baby Essentials", "Baby-Essentials"));

        add(new SearchCategoryPageHeadersAndHrefsCorrespondingValues("Books", "Books", "Books"));
        add(new SearchCategoryPageHeadersAndHrefsCorrespondingValues("Business & Industrial", "Business & Industrial", "Business-Industrial"));
        add(new SearchCategoryPageHeadersAndHrefsCorrespondingValues("Cameras & Photo", "Cameras & Photo", "Cameras-Photo"));

        add(new SearchCategoryPageHeadersAndHrefsCorrespondingValues("Cell Phones, Smart Watches & Accessories", "Cell Phones, Smart Watches & Accessories", "Cell-Phones-Smart-Watches-Accessories"));
        //POSSIBLE BUG (OR FEATURE?) ON EBAY. ACTUAL RESULT : add(new SearchCategoryPageHeadersAndHrefsCorrespondingValues("Cell Phones & Accessories", "Cell Phones, Smart Watches & Accessories", "Cell-Phones-Smart-Watches-Accessories"));


        add(new SearchCategoryPageHeadersAndHrefsCorrespondingValues("Clothing, Shoes & Accessories", "Clothing, Shoes & Accessories", "Clothing-Shoes-Accessories"));
        add(new SearchCategoryPageHeadersAndHrefsCorrespondingValues("Coins & Paper Money", "Coins & Paper Money", "Coins-Paper-Money"));
        add(new SearchCategoryPageHeadersAndHrefsCorrespondingValues("Collectibles", "Collectibles", "Collectibles"));


        add(new SearchCategoryPageHeadersAndHrefsCorrespondingValues("Computers, Tablets & Network Hardware", "Computers, Tablets & Network Hardware", "Computers-Tablets-Network-Hardware"));
        //POSSIBLE BUG (OR FEATURE?) ON EBAY. ACTUAL RESULT : (new SearchCategoryPageHeadersAndHrefsCorrespondingValues("Computers/Tablets & Networking", "Computers, Tablets & Network Hardware", "Computers-Tablets-Network-Hardware"));


        add(new SearchCategoryPageHeadersAndHrefsCorrespondingValues("Consumer Electronics", "Consumer Electronics", "Consumer-Electronics"));

        add(new SearchCategoryPageHeadersAndHrefsCorrespondingValues("Art & Craft Supplies", "Art & Craft Supplies", "Art-Craft-Supplies"));
        //POSSIBLE BUG (OR FEATURE?) ON EBAY. ACTUAL RESULT : add(new SearchCategoryPageHeadersAndHrefsCorrespondingValues("Craft", "Art & Craft Supplies", "Art-Craft-Supplies"));

        add(new SearchCategoryPageHeadersAndHrefsCorrespondingValues("Dolls & Teddy Bears", "Dolls & Teddy Bears", "Dolls-Teddy-Bears"));
        //POSSIBLE BUG (OR FEATURE?) ON EBAY. ACTUAL RESULT : add(new SearchCategoryPageHeadersAndHrefsCorrespondingValues("Dolls & Bears", "Dolls & Teddy Bears", "Dolls-Teddy-Bears"));

        add(new SearchCategoryPageHeadersAndHrefsCorrespondingValues("DVDs & Movies", "DVDs & Movies", "DVDs-Movies"));

        add(new SearchCategoryPageHeadersAndHrefsCorrespondingValues("eBayMotors", "eBayMotors", "eBayMotors"));
        //POSSIBLE BUG (OR FEATURE?) ON EBAY. ACTUAL RESULT: Search By Category "eBayMotors"  with EMPTY SEARCH REQUEST AS OF 16.08.2020 DOESN'T WORK - ERROR: "ERR_TOO_MANY_REDIRECTS"

        add(new SearchCategoryPageHeadersAndHrefsCorrespondingValues("Entertainment Memorabilia", "Entertainment Memorabilia", "Entertainment-Memorabilia"));
        add(new SearchCategoryPageHeadersAndHrefsCorrespondingValues("Gift Cards & Coupons", "Gift Cards & Coupons", "Gift-Cards-Coupons"));
        add(new SearchCategoryPageHeadersAndHrefsCorrespondingValues("Health & Beauty", "Health & Beauty", "Health-Beauty"));


        add(new SearchCategoryPageHeadersAndHrefsCorrespondingValues("Home and Garden", "Home and Garden", "Home-Garden"));
        //POSSIBLE BUG (OR FEATURE?) ON EBAY. ACTUAL RESULT: ANOTHER LAYOUT ON "Home and Garden" SEARCH RESULTS PAGE. NOT THE SAME AS IN MOST OTHER CATEGORIES.

        add(new SearchCategoryPageHeadersAndHrefsCorrespondingValues("Jewelry & Watches", "Jewelry & Watches", "Jewelry-Watches"));
        add(new SearchCategoryPageHeadersAndHrefsCorrespondingValues("Music", "Music", "Music"));
        add(new SearchCategoryPageHeadersAndHrefsCorrespondingValues("Musical Instruments & Gear", "Musical Instruments & Gear", "Musical-Instruments-Gear"));
        add(new SearchCategoryPageHeadersAndHrefsCorrespondingValues("Pet Supplies", "Pet Supplies", "Pet-Supplies"));
        add(new SearchCategoryPageHeadersAndHrefsCorrespondingValues("Pottery & Glass", "Pottery & Glass", "Pottery-Glass"));
        add(new SearchCategoryPageHeadersAndHrefsCorrespondingValues("Real Estate", "Real Estate", "Real-Estate"));
        add(new SearchCategoryPageHeadersAndHrefsCorrespondingValues("Specialty Services", "Specialty Services", "Specialty-Services"));

        add(new SearchCategoryPageHeadersAndHrefsCorrespondingValues("Sporting Goods", "Sporting Goods", "Sporting-Goods"));
        //POSSIBLE BUG (OR FEATURE?) ON EBAY. ACTUAL RESULT: ANOTHER LAYOUT ON SEARCH RESULTS PAGE. NOT THE SAME AS IN MOST OTHER CATEGORIES.

        add(new SearchCategoryPageHeadersAndHrefsCorrespondingValues("Sports Memorabilia, Fan Shop & Sports Cards", "Sports Memorabilia, Fan Shop & Sports Cards", "Sports-Memorabilia-Fan-Shop-Sports-Cards"));
        //POSSIBLE BUG (OR FEATURE?) ON EBAY. ACTUAL RESULT :  add(new SearchCategoryPageHeadersAndHrefsCorrespondingValues("Sports Mem, Cards & Fan Shop", "Sports Memorabilia, Fan Shop & Sports Cards", "Sports-Memorabilia-Fan-Shop-Sports-Cards"));

        add(new SearchCategoryPageHeadersAndHrefsCorrespondingValues("Stamps", "Stamps", "Stamps"));
        add(new SearchCategoryPageHeadersAndHrefsCorrespondingValues("Tickets & Experiences", "Tickets & Experiences", "Tickets-Experiences"));
        add(new SearchCategoryPageHeadersAndHrefsCorrespondingValues("Toys & Hobbies", "Toys & Hobbies", "Toys-Hobbies"));
        add(new SearchCategoryPageHeadersAndHrefsCorrespondingValues("Travel", "Travel", "Travel"));
        add(new SearchCategoryPageHeadersAndHrefsCorrespondingValues("Video Games & Consoles", "Video Games & Consoles", "Video-Games-Consoles"));
        add(new SearchCategoryPageHeadersAndHrefsCorrespondingValues("Everything Else", "Everything Else", "Everything-Else"));


    }};


    @Test
    //THIS TEST CHECKS ALL  "searchCategoryName"-s FROM  "EXPECTED_SEARCH_CATEGORY_PAGE_HEADERS_AND_HREFS_CORRESPONDING_VALUES_LIST"  AND IS GOING TO FAIL BECAUSE OF POSSIBLE BUGS (OR FEATURES?) ON EBAY
    //THE first test where i=0 (that is All Categories) is going to fail as well because of url mismatch and particular layout of "All Categories Search Results Page" (not as like in most other categories)
    //FOR DEMONSTRATIVE PURPOSES  (i=1) (i is set to 1) - search starts from the second search option "Antiques", goes well  and then fails when reaching "Baby Essentials"
    public void checkPageHeaderOnSearchResultsPageEqualsSelectedCategoryNameUsingEmptySearchRequest() {


        Select searchCategorySelect = new Select(getHomePage().getSearchCategorySelect());

        Assert.assertEquals(searchCategorySelect.getOptions().size(), EXPECTED_SEARCH_CATEGORY_PAGE_HEADERS_AND_HREFS_CORRESPONDING_VALUES_LIST.size());

        for (int i = 1; i < searchCategorySelect.getOptions().size(); i++) {

            searchCategorySelect.selectByIndex(i);

            String selectedSearchCategoryName = searchCategorySelect.getOptions().get(i).getText();

            getHomePage().clickOnSearchButton();

            Assert.assertTrue(getDriver().getCurrentUrl().contains(EXPECTED_SEARCH_CATEGORY_PAGE_HEADERS_AND_HREFS_CORRESPONDING_VALUES_LIST.get(i).hrefContains), "URL MISMATCH");

            if (getSearchResultsPage().ifSearchCategoryPageHeaderSpanExists()) {
                String actualPageHeaderName = getSearchResultsPage().getSearchCategoryPageHeaderSpan().getText();

                Assert.assertEquals(actualPageHeaderName, selectedSearchCategoryName, "Expected Page Header according to Selected Category Name value mismatches Actual Page Header value");

            } else {
                Assert.fail("Page Header Span '" + selectedSearchCategoryName + "' doesn't exist on a Search Results Page");
            }

            getBasePage().goToPreviousPage();

            Assert.assertTrue(getDriver().getCurrentUrl().equals("https://www.ebay.com/"));

        }


    }


}

package tests;

import org.openqa.selenium.Keys;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;

public class SearchTests extends BaseTest {


    private String SEARCH_CATEGORY_NAME = "Books";

    @Test(priority = 4)
    public void checkPageHeaderOnSearchResultsPageEqualsSelectedCategoryNameUsingEmptySearchRequestAndParticularSearchCategoryName() {

        Select searchCategorySelect = new Select(getHomePage().getSearchCategorySelect());
        searchCategorySelect.selectByVisibleText(SEARCH_CATEGORY_NAME);
        getHomePage().clickOnSearchButton();

        Assert.assertTrue(getDriver().getCurrentUrl().contains(SEARCH_CATEGORY_NAME), "URL MISMATCH");

        if (getSearchResultsPage().ifSearchCategoryPageHeaderSpanExists()) {
            String actualPageHeaderName = getSearchResultsPage().getSearchCategoryPageHeaderSpan().getText();

            Assert.assertEquals(actualPageHeaderName, SEARCH_CATEGORY_NAME, "Expected Page Header according to Selected Category Name value mismatches Actual Page Header value");

        } else {
            Assert.fail("Page Header Span '" + SEARCH_CATEGORY_NAME + "' doesn't exist on a Search Results Page");
        }

    }


}

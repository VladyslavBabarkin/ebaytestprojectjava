package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import pages.*;

public class BaseTest {

    private WebDriver driver;
    private static final String EBAY_URL = "https://www.ebay.com/";


    @BeforeTest
    public void profileSetUp() {
        System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\chromedriver.exe");
    }



    @BeforeMethod
    public void testsSetUp() {
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get(EBAY_URL);
        getBasePage().waitForPageLoadComplete(30);
    }

    @AfterMethod
    public void tearDown() {
        driver.quit();
    }



    public WebDriver getDriver() {
        return driver;
    }

    public BasePage getBasePage() {
        return new BasePage(getDriver());
    }

    public HomePage getHomePage() {
        return new HomePage(getDriver());
    }

    public RegistrationPage getRegistrationPage() {
        return new RegistrationPage(getDriver());
    }

    public SignInPage getSignInPage() {
        return new SignInPage(getDriver());
    }

    public CameraDronesPage getCameraDronesPage() {
        return new CameraDronesPage(getDriver());
    }

    public GlobalDailyDealsPage getGlobalDailyDealsPage() {
        return new GlobalDailyDealsPage(getDriver());
    }

    public ProductItemPage getProductItemPage() {
        return new ProductItemPage(getDriver());
    }

    public SearchResultsPage getSearchResultsPage() {
        return new SearchResultsPage(getDriver());
    }

    public ShoppingCartPage getShoppingCartPage() {
        return new ShoppingCartPage(getDriver());
    }

    public AdvancedSearchPage getAdvancedSearchPage() {
        return new AdvancedSearchPage(getDriver());
    }

}

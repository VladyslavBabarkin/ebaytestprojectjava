package tests;

import org.testng.Assert;
import org.testng.annotations.Test;

public class SignInTests extends BaseTest {


    private String INVALID_USER_ID = "ebayuser";
    private String EXPECTED_SIGN_IN_ERROR_MESSAGE = "Oops, that's not a match.";


    @Test(priority = 1)
    public void checkSignInErrorMessageValueAppearsWhileSigningInWithInvalidUserId() {

        getHomePage().clickOnSignInButton();
        getBasePage().waitVisibilityOfElement(30, getSignInPage().getUserIdInputTextField());
        Assert.assertFalse(getSignInPage().ifSignInErrorMessageParagraphExist());
        getSignInPage().typeTextInUserIdInputTextField(INVALID_USER_ID);
        getSignInPage().clickOnSignInContinueButton();
        getBasePage().waitVisibilityOfElement(30, getSignInPage().getSignInErrorMessageParagraph());
        Assert.assertTrue(getSignInPage().ifSignInErrorMessageParagraphExist());
        Assert.assertTrue(getSignInPage().getSignInErrorMessageParagraph().isDisplayed());
        Assert.assertEquals(getSignInPage().getSignInErrorMessageParagraph().getText(), EXPECTED_SIGN_IN_ERROR_MESSAGE);

    }

}
